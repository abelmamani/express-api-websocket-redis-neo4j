require('dotenv').config();
const cors = require('cors');
const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const Redis = require('ioredis');
const neo4j = require('neo4j-driver');

const app = express();
const server = http.createServer(app);
const io = socketIo(server , {
    cors: {origin: "http://localhost:5173"}
});
const subscriber = new Redis(); // Instancia para suscripción
const publisher = new Redis(); // Instancia para publicación
// Conexión a la base de datos Neo4j en Aura Cloud
const neo4jDriver = neo4j.driver(process.env.NEO4J_URI, neo4j.auth.basic(process.env.NEO4J_USERNAME, process.env.NEO4J_PASSWORD));
const neo4jSession = neo4jDriver.session();

// Configura tus rutas y eventos WebSocket aquí
app.use(cors());

// Rutas de Express (agrega tus rutas)
app.get('/bus_stops', async (req, res) => {
  try {
      const query = "MATCH (busStop:BusStop) RETURN busStop";
      const result = await neo4jSession.run(query);
      const busStops = result.records.map(record => {
          return record.get("busStop").properties;
      });

      res.json({ busStops });
  } catch (error) {
      res.status(500).json({ error: "Error al consultar categorías" });
  }
});

app.get('/nearest_bus_stop', async (req, res) => {
  try {
    // Obtén las coordenadas de origen desde la solicitud GET
    const latitude = parseFloat(req.query.latitude);
    const longitude = parseFloat(req.query.longitude);
    if (isNaN(latitude) || isNaN(longitude)) {
      return res.status(400).json({ error: "Coordenadas inválidas" });
    }
    const query = `
      WITH point({ latitude: $latitude, longitude: $longitude }) as currentLocation
      MATCH (busStop:BusStop)
      WITH currentLocation, busStop, point({ latitude: busStop.latitude, longitude: busStop.longitude }) as stopLocation
      RETURN busStop, point.distance(currentLocation, stopLocation) AS distance
      ORDER BY distance ASC
      LIMIT 10
    `;
    const result = await neo4jSession.run(query, {
      latitude: latitude, longitude: longitude
    });

    const busStops = result.records.map(record => {
      const busStopProperties = record.get("busStop").properties;
      const distance = record.get("distance");
      return {
        busStop: busStopProperties,
        distance: distance
      };
    });

      res.json({ busStops});
  } catch (error) {
    console.log(error)
      res.status(500).json({ error: "Error al consultar los bus stops mas cercanos" });
  }
});
// Ruta de ejemplo para consultar categorías desde Neo4j
app.get('/categories', async (req, res) => {
  try {
      const query = "MATCH (c:Category) RETURN c";
      const result = await neo4jSession.run(query);
      const categories = result.records.map(record => {
          return record.get("c").properties;
      });

      res.json({ categories });
  } catch (error) {
      res.status(500).json({ error: "Error al consultar categorías" });
  }
});

// Suscríbete a un canal Redis para recibir ubicaciones de conductores
subscriber.subscribe('ubicacionConductor', (err, count) => {
  if (err) {
    throw err;
  }
  console.log(`Suscripto a ${count} canales`);
});

// Maneja los mensajes del canal Redis
subscriber.on('message', (channel, message) => {
  // Envía el mensaje (ubicación del conductor) a todos los clientes conectados a través de WebSocket
  io.emit(channel, message);
});


// Manejo de conexiones WebSocket

io.on('connection', (socket) => {
  console.log('Cliente conectado:', socket.id);
  // Escucha eventos de los conductores para enviar su ubicación
  socket.on('enviarUbicacion', (ubicacion) => {
    // Publica la ubicación en el canal Redis
    publisher.publish('ubicacionConductor', JSON.stringify(ubicacion));
  });
  // Cierra la conexión cuando sea necesario (agrega tu lógica)
  socket.on('disconnect', () => {
    console.log('Cliente desconectado:', socket.id);
  });
});

server.listen(3000, () => {
  console.log('Servidor express en el puerto 3000');
});
